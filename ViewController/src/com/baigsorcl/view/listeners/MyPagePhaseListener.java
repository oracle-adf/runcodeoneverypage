package com.baigsorcl.view.listeners;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.controller.v2.lifecycle.PagePhaseEvent;
import oracle.adf.controller.v2.lifecycle.PagePhaseListener;


public class MyPagePhaseListener implements PagePhaseListener {


    public MyPagePhaseListener() {
        super();
    }

    public void afterPhase(PagePhaseEvent pagePhaseEvent) {

       
        if (pagePhaseEvent.getPhaseId() == Lifecycle.PREPARE_RENDER_ID) {
            FacesContext fctx = FacesContext.getCurrentInstance();
            String viewId = fctx.getViewRoot().getViewId();
            FacesMessage message = new FacesMessage("Hello Page no " + viewId);
            fctx.addMessage(null, message);
        }
    }

    public void beforePhase(PagePhaseEvent pagePhaseEvent) {


    }

}
